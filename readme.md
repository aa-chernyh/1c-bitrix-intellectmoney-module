#Модуль оплаты платежной системы IntellectMoney для CMS 1С-Битрикс

> **Внимание!** <br>
Данная версия актуальна на *23 апреля 2020 года* и не обновляется. <br>
Актуальную версию можно найти по ссылке https://wiki.intellectmoney.ru/pages/viewpage.action?pageId=557468#bitrix-files.

Страница модуля на marketplace: https://marketplace.1c-bitrix.ru/solutions/im.payment/
<br>
Инструкция по настройке доступна по ссылке https://wiki.intellectmoney.ru/pages/viewpage.action?pageId=557468#setup
<br>
Ответы на частые вопросы можно найти здесь: https://wiki.intellectmoney.ru/pages/viewpage.action?pageId=557468#faq
