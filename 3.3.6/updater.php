<?php

if (IsModuleInstalled("im.payment")) {
    DeleteDirFilesEx('/bitrix/php_interface/include/sale_payment/im.payment/');
    $updater->CopyFiles("install/payment", "/bitrix/php_interface/include/sale_payment/im.payment/");
    DeleteDirFilesEx('/bitrix/modules/im.payment');
} else {
    DeleteDirFilesEx('/bitrix/modules/im.payment');
    DeleteDirFilesEx('/bitrix/php_interface/include/sale_payment/im.payment/');
}
?>