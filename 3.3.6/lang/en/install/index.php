<?php
$MESS["IM.PAYMENT_INSTALL_TITLE"] = "Installation of IntellectMoney payment system module";
$MESS["IM.PAYMENT_MODULE_NAME"] = "IntellectMoney payment system";
$MESS["IM.PAYMENT_MODULE_DESCRIPTION"] = "Accept payments via credit cards, SberBank Online, mobile commerce, payment terminals";
$MESS["IM.PAYMENT_PARTNER_NAME"] = "IntellectMoney";

$MESS["IM.PAYMENT_INSTALL_ERROR_TITLE"] = "You can not install a payment module IntellectMoney";
$MESS["IM.PAYMENT_INSTALL_ERROR_DESCRIPTION"] = "To install IntellectMoney payment module requires a module Online store (sale) version higher 17.0.0 and version Bitrix higher 17.0.9";
$MESS["IM.PAYMENT_INSTALL_ERROR_BUTTON"] = "Back";

?>