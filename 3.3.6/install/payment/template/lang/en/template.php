<?php
$MESS["IM.PAYMENT_FOR_HEADER"] = "Pay via <span style='font-weight: bold'>IntellectMoney</span><br><br>To paid : #AMOUNT#";
$MESS["IM.PAYMENT_FOR_DESCRIPTION"] = "Automatic transition to the payment page in";
$MESS["IM.PAYMENT_FOR_SHORT_DESCRIPTION"] = "Automatic transition to the payment page";
$MESS["IM.PAYMENT_PAY_BUTTON"] = "To pay";