<?php
$MESS["IM.PAYMENT_FOR_HEADER"] = "Оплата в системе <span style='font-weight: bold'>IntellectMoney</span><br><br>сумма к оплате : #AMOUNT#";
$MESS["IM.PAYMENT_FOR_DESCRIPTION"] = "Автоматический переход на страницу оплаты через";
$MESS["IM.PAYMENT_FOR_SHORT_DESCRIPTION"] = "Автоматический переход на страницу оплаты";
$MESS["IM.PAYMENT_PAY_BUTTON"] = "Оплатить";