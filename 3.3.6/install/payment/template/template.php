<?php

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
?>
<style>
    .intellectmoney-paysystem-wrapper{
        position: relative;
        padding: 24px 38px 24px 38px;
        margin: 0 -15px 0 0;
        border: 1px solid #3bc8f5;
        font: 14px "Helvetica Neue",Arial,Helvetica,sans-serif;
        color: #424956;
        display: block;
    }
    .button{
        display: inline-block;
        margin: 26px 10px 26px 0;
        padding: 0 22px;
        height: 38px;
        border: 0;
        border-radius: 2px;
        background: #f59023;
        font: bold 13px/35px "Helvetica Neue",Arial,Helvetica,sans-serif;
        color: #fff;
        transition: background .3s ease;
    }
    .button:hover {
        background: #ff911a;
    }
    .description{
        display: inline-block;
        margin: 0 0 15px 0;
        font: 12px "Helvetica Neue",Arial,Helvetica,sans-serif;
        color: #80868e;
    }
</style>
<div class="intellectmoney-paysystem-wrapper">
    <div class=""> <?= Loc::getMessage("IM.PAYMENT_FOR_HEADER", array("#AMOUNT#" => $this->recipient_amount)) ?>
    </div>
    <form id="pay" name="pay" method="POST" action="https://merchant.intellectmoney.ru/<?= $this->lang; ?>/">
        <?php if (!empty($this->invoice_id)) { ?>
            <input type="hidden" name="invoiceId" value="<?= $this->invoice_id; ?>">
        <?php } else { ?>
            <input type="hidden" name="eshopId" value="<?= $this->eshop_id; ?>">
            <input type="hidden" name="orderId" value="<?= $this->order_id; ?>">
            <input type="hidden" name="serviceName" value="<?= $this->service_name; ?>">
            <input type="hidden" name="recipientAmount" value="<?= $this->recipient_amount; ?>">
            <input type="hidden" name="recipientCurrency" value="<?= $this->recipient_currency; ?>">
            <input type="hidden" name="user_email" value="<?= $this->user_email; ?>">
            <input type="hidden" name="userName" value="<?= $this->user_name; ?>">
            <input type="hidden" name="successUrl" value="<?= $this->success_url; ?>">
            <input type="hidden" name="preference" value="<?= $this->preference; ?>">
            <?php if ($this->hold_mode != "NONE") { ?>
                <input type="hidden" name="holdMode" value="<?= $this->hold_mode; ?>">
                <?php if ($this->hold_time) { ?>
                    <input type="hidden" name="holdTime" value="<?= $this->hold_time; ?>">
                <?php } ?>
				<?php if ($this->expire_date) { ?>
            		<input type="hidden" name="expireDate" value="<?= $this->expire_date; ?>">
				<?php } ?>
            <?php } ?>
            <input type="hidden" name="merchantReceipt" value="<?= $this->merchant_receipt; ?>">
            <input type="hidden" name="hash" value="<?= $this->pre_hash; ?>">
            <?php if (!empty($this->conversion_rate)) { ?>
                <input type="hidden" name="UserField_1" value="<?= $this->default_currency; ?>">
                <input type="hidden" name="UserFieldName_1" value="DefaultCurrency">
                <input type="hidden" name="UserField_2" value="<?= $this->conversion_rate; ?>">
                <input type="hidden" name="UserFieldName_2" value="ConversionRate">
            <?php } ?>
        <?php } ?>
            <input class="button" name="BuyButton" value="<?= Loc::getMessage("IM.PAYMENT_PAY_BUTTON") ?>" type="submit">
        <?php
            $prefix = ($this->delay == 0) ? "SHORT_" : "";
            $description = "IM.PAYMENT_FOR_" . $prefix . "DESCRIPTION";
        ?>
        <span class="description"><?= Loc::getMessage($description); ?>: <span id="timer"></span></span>
    </form>
</div>
<script type='text/javascript'>

    function submit() {
        var timerResult = document.getElementById('timer');
        var timerVal = <?= intval($this->delay); ?>;
        var timerId = setInterval(function () {
            timerResult.textContent = timerVal;
            timerVal--;
            if (timerVal <= 0) {
                clearInterval(timerId);
                document.getElementById('pay').submit();
            }
        }, 1000);
    }
    document.onload = submit();
</script>
