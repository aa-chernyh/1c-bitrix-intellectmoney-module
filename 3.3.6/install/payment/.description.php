<?php

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

$psDescription = Loc::getMessage("IM.PAYMENT_DESC");
$psTitle = Loc::getMessage("IM.PAYMENT_TITLE");

$taxValues = array(
    "1" => "20%",
    "2" => "10%",
    "3" => Loc::getMessage("IM.PAYMENT_TAX_DESC") . " 20/120",
    "4" => Loc::getMessage("IM.PAYMENT_TAX_DESC") . " 10/110",
    "5" => "0%",
    "6" => Loc::getMessage("IM.PAYMENT_TAX_NONE")
);
$all_status_id = CSaleStatus::GetList(array(), array(), array("ID", "COUNT" => "LID"), array(), array("ID"));
$all_status = array();
foreach ($all_status_id->arResult as $status) {
    $status = CSaleStatus::GetByID($status["ID"], LANGUAGE_ID);
    if (!empty($status)) {
        $status_info = CSaleStatus::GetByID($status["ID"], LANGUAGE_ID);
    } else {
        $status_info = array("ID" => $status["ID"], "NAME" => $status["ID"]);
    }

    $all_status[$status_info["ID"]] = $status_info["NAME"];
}

$data = array(
    "CODES" => array(
        "IM.ESHOP_ID" => array(
            "NAME" => Loc::getMessage("IM.PAYMENT_OPTIONS_ESHOP_ID"),
            "DESCRIPTION" => Loc::getMessage("IM.PAYMENT_OPTIONS_ESHOP_ID_DESC"),
            "SORT" => 50,
            "GROUP" => Loc::getMessage("IM.PAYMENT_GENERAL_GROUP"),
        ),
        "IM.SEKRET_KEY" => array(
            "NAME" => Loc::getMessage("IM.PAYMENT_OPTIONS_SECRET_KEY"),
            "DESCRIPTION" => Loc::getMessage("IM.PAYMENT_OPTIONS_SECRET_KEY_DESC"),
            "SORT" => 100,
            "GROUP" => Loc::getMessage("IM.PAYMENT_GENERAL_GROUP"),
        ),
        "IM.SUCCESS_URL" => array(
            "NAME" => Loc::getMessage("IM.PAYMENT_SUCCESS_URL"),
            "DESCRIPTION" => Loc::getMessage("IM.PAYMENT_SUCCESS_URL_DESC"),
            "SORT" => 150,
            "GROUP" => Loc::getMessage("IM.PAYMENT_GENERAL_GROUP"),
        ),
        "IM.PREFERENCE" => array(
            "NAME" => Loc:: getMessage("IM.PAYMENT_PREFERENCE"),
            "DESCRIPTION" => Loc::getMessage("IM.PAYMENT_PREFERENCE_DESC"),
            "SORT" => 200,
            "GROUP" => Loc::getMessage("IM.PAYMENT_GENERAL_GROUP"),
        ),
        "IM.LANGUAGE" => array(
            "NAME" => Loc::getMessage("IM.PAYMENT_LANGUAGE_TITLE"),
            "SORT" => 250,
            "GROUP" => Loc::getMessage("IM.PAYMENT_GENERAL_GROUP"),
            "INPUT" => array(
                "TYPE" => "ENUM",
                "OPTIONS" => array(
                    "RU" => Loc::getMessage("IM.PAYMENT_LANGUAGE_RU"),
                    "EN" => Loc::getMessage("IM.PAYMENT_LANGUAGE_EN"),
                    "DE" => Loc::getMessage("IM.PAYMENT_LANGUAGE_DE"),
                    "FR" => Loc::getMessage("IM.PAYMENT_LANGUAGE_FR"),
                    "IT" => Loc::getMessage("IM.PAYMENT_LANGUAGE_IT"),
                    "JP" => Loc::getMessage("IM.PAYMENT_LANGUAGE_JP"),
                    "BG" => Loc::getMessage("IM.PAYMENT_LANGUAGE_BG"),
                    "ES" => Loc::getMessage("IM.PAYMENT_LANGUAGE_ES"),
                    "PT" => Loc::getMessage("IM.PAYMENT_LANGUAGE_PT"),
                )
            ),
            "DEFAULT" => array(
                "PROVIDER_KEY" => "VALUE",
                "PROVIDER_VALUE" => "RU"
            )
        ),
        "IM.IS_TEST" => array(
            "NAME" => Loc::getMessage("IM.PAYMENT_IS_TEST"),
            "SORT" => 300,
            "GROUP" => Loc::getMessage("IM.PAYMENT_GENERAL_GROUP"),
            "INPUT" => array(
                "TYPE" => "Y/N"
            )
        ),
        "IM.SUBMIT_DELAY" => array(
            "NAME" => Loc::getMessage("IM.PAYMENT_SUBMIT_DELAY"),
            "DESCRIPTION" => Loc::getMessage("IM.PAYMENT_SUBMIT_DELAY_DESCRIPTION"),
            "SORT" => 325,
            "GROUP" => Loc::getMessage("IM.PAYMENT_GENERAL_GROUP"),
            "DEFAULT" => array(
                "PROVIDER_KEY" => "VALUE",
                "PROVIDER_VALUE" => "5"
            )
        ),
        "IM.HOLD_MODE" => array(
            "NAME" => Loc::getMessage("IM.PAYMENT_HOLD_MODE"),
            "DESCRIPTION" => Loc::getMessage("IM.PAYMENT_HOLD_MODE_DESCRIPTION"),
            "SORT" => 350,
            "GROUP" => Loc::getMessage("IM.PAYMENT_HOLD_GROUP"),
            "INPUT" => array(
                "TYPE" => "ENUM",
                "OPTIONS" => array(
                    "0" => Loc::getMessage("IM.PAYMENT_HOLD_MODE_OFF"),
                    "1" => Loc::getMessage("IM.PAYMENT_HOLD_MODE_ON"),
                    "PERSONAL" => Loc::getMessage("IM.PAYMENT_HOLD_MODE_PERSONAL"),
                )
            ),
            "DEFAULT" => array(
                "PROVIDER_KEY" => "VALUE",
                "PROVIDER_VALUE" => "0"
            )
        ),
        "IM.EXPIRE_DATE" => array(
            "NAME" => Loc::getMessage("IM.PAYMENT_EXPIRE_DATE"),
            "DESCRIPTION" => Loc::getMessage("IM.PAYMENT_EXPIRE_DATE_DESCRIPTION"),
            "SORT" => 400,
            "GROUP" => Loc::getMessage("IM.PAYMENT_HOLD_GROUP"),
            "DEFAULT" => array(
                "PROVIDER_KEY" => "VALUE",
                "PROVIDER_VALUE" => ""
            )
        ),
        "IM.HOLD_TIME" => array(
            "NAME" => Loc::getMessage("IM.PAYMENT_HOLD_TIME"),
            "DESCRIPTION" => Loc::getMessage("IM.PAYMENT_HOLD_TIME_DESCRIPTION"),
            "SORT" => 450,
            "GROUP" => Loc::getMessage("IM.PAYMENT_HOLD_GROUP"),
            "DEFAULT" => array(
                "PROVIDER_KEY" => "VALUE",
                "PROVIDER_VALUE" => ""
            )
        ),
        "IM.TAX" => array(
            "NAME" => Loc::getMessage("IM.PAYMENT_TAX"),
            "DESCRIPTION" => Loc::getMessage("IM.PAYMENT_TAX_DESCRIPTION"),
            "SORT" => 550,
            "GROUP" => Loc::getMessage("IM.PAYMENT_MERCHANT_RECEIPT_GROUP"),
            "INPUT" => array(
                "TYPE" => "ENUM",
                "OPTIONS" => $taxValues
            ),
            "DEFAULT" => array(
                "PROVIDER_KEY" => "VALUE",
                "PROVIDER_VALUE" => "1"
            )
        ),
        "IM.DELIVERY_TAX" => array(
            "NAME" => Loc::getMessage("IM.PAYMENT_DELIVERY_TAX"),
            "DESCRIPTION" => Loc::getMessage("IM.PAYMENT_DELIVERY_TAX_DESCRIPTION"),
            "SORT" => 600,
            "GROUP" => Loc::getMessage("IM.PAYMENT_MERCHANT_RECEIPT_GROUP"),
            "INPUT" => array(
                "TYPE" => "ENUM",
                "OPTIONS" => $taxValues
            ),
            "DEFAULT" => array(
                "PROVIDER_KEY" => "VALUE",
                "PROVIDER_VALUE" => "1"
            )
        ),
        "IM.GROUP" => array(
            "NAME" => Loc::getMessage("IM.PAYMENT_GROUP"),
            "DESCRIPTION" => Loc::getMessage("IM.PAYMENT_GROUP_DESCRIPTION"),
            "SORT" => 650,
            "GROUP" => Loc::getMessage("IM.PAYMENT_MERCHANT_RECEIPT_GROUP"),
            "DEFAULT" => array(
                "PROVIDER_KEY" => "VALUE",
                "PROVIDER_VALUE" => ""
            )
        ),
        "IM.ORDER_ID" => array(
            "NAME" => Loc::getMessage("IM.PAYMENT_ORDER_ID"),
            "DESCRIPTION" => Loc::getMessage("IM.PAYMENT_ORDER_ID_DESC"),
            "SORT" => 700,
            "GROUP" => Loc::getMessage("IM.PAYMENT_ORDER_GROUP"),
            "DEFAULT" => array(
                "PROVIDER_KEY" => "PAYMENT",
                "PROVIDER_VALUE" => "ID"
            )
        ),
        "IM.EMAIL" => array(
            "NAME" => Loc::getMessage("IM.PAYMENT_EMAIL"),
            "SORT" => 800,
            "GROUP" => Loc::getMessage("IM.PAYMENT_ORDER_GROUP"),
            "DEFAULT" => array(
                "PROVIDER_KEY" => "USER",
                "PROVIDER_VALUE" => "EMAIL"
            )
        ),
        "IM.PAYMENT_CREATE_STATUS" => array(
            "NAME" => Loc::getMessage("IM.PAYMENT_CREATE_STATUS"),
            "DESCRIPTION" => Loc::getMessage("IM.PAYMENT_CREATE_STATUS_DESCRIPTION"),
            "SORT" => 850,
            "GROUP" => Loc::getMessage("IM.PAYMENT_ORDER_GROUP"),
            "INPUT" => array(
                "TYPE" => "ENUM",
                "OPTIONS" => $all_status
            )
        ),
        "IM.PAYMENT_PAYEED_STATUS" => array(
            "NAME" => Loc::getMessage("IM.PAYMENT_PAYEED_STATUS"),
            "DESCRIPTION" => Loc::getMessage("IM.PAYMENT_PAYEED_STATUS_DESCRIPTION"),
            "SORT" => 900,
            "GROUP" => Loc::getMessage("IM.PAYMENT_ORDER_GROUP"),
            "INPUT" => array(
                "TYPE" => "ENUM",
                "OPTIONS" => $all_status
            )
        ),
        "IM.PAYMENT_HOLD_STATUS" => array(
            "NAME" => Loc::getMessage("IM.PAYMENT_HOLD_STATUS"),
            "DESCRIPTION" => Loc::getMessage("IM.PAYMENT_HOLD_STATUS_DESCRIPTION"),
            "SORT" => 950,
            "GROUP" => Loc::getMessage("IM.PAYMENT_ORDER_GROUP"),
            "INPUT" => array(
                "TYPE" => "ENUM",
                "OPTIONS" => $all_status
            )
        ),
        "IM.PAYMENT_PARTIALLY_PAYEED_STATUS" => array(
            "NAME" => Loc::getMessage("IM.PAYMENT_PARTIALLY_PAYEED_STATUS"),
            "DESCRIPTION" => Loc::getMessage("IM.PAYMENT_PARTIALLY_PAYEED_STATUS_DESCRIPTION"),
            "SORT" => 1000,
            "GROUP" => Loc::getMessage("IM.PAYMENT_ORDER_GROUP"),
            "INPUT" => array(
                "TYPE" => "ENUM",
                "OPTIONS" => $all_status
            )
        ),
        "IM.PAYMENT_CANSELED_STATUS" => array(
            "NAME" => Loc::getMessage("IM.PAYMENT_CANSELED_STATUS"),
            "DESCRIPTION" => Loc::getMessage("IM.PAYMENT_CANSELED_STATUS_DESCRIPTION"),
            "SORT" => 1050,
            "GROUP" => Loc::getMessage("IM.PAYMENT_ORDER_GROUP"),
            "INPUT" => array(
                "TYPE" => "ENUM",
                "OPTIONS" => $all_status
            )
        ),
    )
);
