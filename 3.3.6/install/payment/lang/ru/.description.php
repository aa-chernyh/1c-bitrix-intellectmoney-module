<?php
$MESS["IM.PAYMENT_TITLE"] = "������� IntellectMoney";
$MESS["IM.PAYMENT_DESC"] = "����� ������ �� ����������� ������, ����������� ���������, ��������� ���������";

$MESS["IM.PAYMENT_ORDER_ID"] = "��� ������";
$MESS["IM.PAYMENT_ORDER_ID_DESC"] = "����� ������ � ����� ��������-��������";

$MESS["IM.PAYMENT_ORDER_DATE"] = "���� ������";
$MESS["IM.PAYMENT_ORDER_DATE_DESC"] = "";

$MESS["IM.PAYMENT_ORDER_SUM"] = "����� � ������";
$MESS["IM.PAYMENT_ORDER_SUM_DESC"] = "�������� ����� ����� ������ ������ ��������";

$MESS["IM.PAYMENT_SUCCESS_URL"] = "����� ��� �������� ������";
$MESS["IM.PAYMENT_SUCCESS_URL_DESC"] = "URL (�� ���-����� ��������), �� ������� ����� ������������� ���������� � ������ ��������� ���������� �������. URL ������ ����� ������� http:// ��� https://";

$MESS["IM.PAYMENT_PREFERENCE"] = "��������� ������� ������";
$MESS["IM.PAYMENT_PREFERENCE_DESC"] = "������� ������, ������� ����� �������� ��� ������ ��� ������ �������. ��������� �������� (��� ������ ���������� ��� ����������� �������): inner, bankcard, exchangers. ��������� https://docs.google.com/document/d/1VHIkEyNgdeyqmrcVuGx0Lv8xrZp2XJZ9nYNUHDYvw0k/edit";

$MESS["IM.PAYMENT_EMAIL"] = "Email";

$MESS["IM.PAYMENT_IS_TEST"] = "�������� �����";

$MESS["IM.PAYMENT_HOLD_MODE"] = "����� ������������";
$MESS["IM.PAYMENT_HOLD_MODE_DESCRIPTION"] = "��������, ���� ������ ������������ ������������ �������� ������� ��� ������";
$MESS["IM.PAYMENT_HOLD_MODE_OFF"] = "�� ����������� ����";
$MESS["IM.PAYMENT_HOLD_MODE_ON"] = "����������� ����";
$MESS["IM.PAYMENT_HOLD_MODE_PERSONAL"] = "������ � ������ ��������";
$MESS["IM.PAYMENT_EXPIRE_DATE"] = "���� ����� �����";
$MESS["IM.PAYMENT_EXPIRE_DATE_DESCRIPTION"] = "�������� �� ���� ����� �����. ���� � ������� ����� ����� ����� �� �� ����� ������� - ���� ������������ ����������. ��������� � �����. ������������ �������� 180 ���� (4319 �����), �� ��������� 180 ����. ��� ���������� ��������� '����� ������������' ������������ �������� 119 ����� �� ��������� 72 ����";
$MESS["IM.PAYMENT_HOLD_TIME"] = "���� ������������ �������� �������";
$MESS["IM.PAYMENT_HOLD_TIME_DESCRIPTION"] = "��� ���������� ��������� '����� ������������' �������� �� ���� ���������� �������� �������. ��������� � �����. ������������ �������� 119 ����� �� ��������� 72 ����";

$MESS["IM.PAYMENT_OPTIONS_ESHOP_ID"] = "����� �������� � ������� IntellectMoney";
$MESS["IM.PAYMENT_OPTIONS_ESHOP_ID_DESC"] = "������ � ������ �������� �� ����� htttp://intellectmoney.ru � ������� ��������";

$MESS["IM.PAYMENT_OPTIONS_SECRET_KEY"] = "��������� ����";
$MESS["IM.PAYMENT_OPTIONS_SECRET_KEY_DESC"] = "��������������� ��� �������� �������� � ������� IntellectMoney";

$MESS["IM.PAYMENT_OPTIONS_VAT_RATE"] = "������ ��� ��� �������";
$MESS["IM.PAYMENT_OPTIONS_VAT_RATE_DESC"] = "������� �� ����� ������ ��� �� ���������";

$MESS["IM.PAYMENT_LANGUAGE_TITLE"] = "���� ���������� � ������� IntellectMoney";
$MESS["IM.PAYMENT_LANGUAGE_RU"] = "�������";
$MESS["IM.PAYMENT_LANGUAGE_EN"] = "����������";
$MESS["IM.PAYMENT_LANGUAGE_DE"] = "��������";
$MESS["IM.PAYMENT_LANGUAGE_FR"] = "�����������";
$MESS["IM.PAYMENT_LANGUAGE_IT"] = "�����������";
$MESS["IM.PAYMENT_LANGUAGE_ES"] = "���������";
$MESS["IM.PAYMENT_LANGUAGE_PT"] = "�������������";
$MESS["IM.PAYMENT_LANGUAGE_JP"] = "��������";
$MESS["IM.PAYMENT_LANGUAGE_BG"] = "����������";

$MESS["IM.PAYMENT_SUBMIT_DELAY"] = "�������� ����� ��������� �����";
$MESS["IM.PAYMENT_SUBMIT_DELAY_DESCRIPTION"] = "������� � �������� ����� �������� �������� ��������� �����. �� ��������� 5 ������";

$MESS["IM.PAYMENT_TAX_DESC"] = "����.";
$MESS["IM.PAYMENT_TAX_NONE"] = "�� ����������";
$MESS["IM.PAYMENT_TAX"] = "������ ��� ��� �������";
$MESS["IM.PAYMENT_TAX_DESCRIPTION"] = "������� �� ����� ������ ��� �� ���������";
$MESS["IM.PAYMENT_DELIVERY_TAX"] = "������ ��� ��� ��������";
$MESS["IM.PAYMENT_DELIVERY_TAX_DESCRIPTION"] = "������� �� ����� ������ ��� �������������� ��������";
$MESS["IM.PAYMENT_GROUP"] = "������ ���������";
$MESS["IM.PAYMENT_GROUP_DESCRIPTION"] = "������ ��������� � ������� ������� ����� ������ ���. ���� ������ �������� �������� ������, ����� ����������� �������� �� ���������";

$MESS["IM.PAYMENT_GENERAL_GROUP"] = "��������� �������� ������";
$MESS["IM.PAYMENT_HOLD_GROUP"] = "���� ����� ����� � ������������";
$MESS["IM.PAYMENT_MERCHANT_RECEIPT_GROUP"] = "��������� ������ �����";
$MESS["IM.PAYMENT_ORDER_GROUP"] = "��������� ������";

$MESS["IM.PAYMENT_CREATE_STATUS"] = "���� ������";
$MESS["IM.PAYMENT_CREATE_STATUS_DESCRIPTION"] = "������� � ����� ������ ���������� �����, ����� ���� � ������� IntellectMoney ������";
$MESS["IM.PAYMENT_PAYEED_STATUS"] = "���� �������";
$MESS["IM.PAYMENT_PAYEED_STATUS_DESCRIPTION"] = "������� � ����� ������ ���������� �����, ����� ���� � ������� IntellectMoney ��������� �������";
$MESS["IM.PAYMENT_HOLD_STATUS"] = "���� ������������";
$MESS["IM.PAYMENT_HOLD_STATUS_DESCRIPTION"] = "������� � ����� ������ ���������� �����, ����� ���� � ������� IntellectMoney ������������";
$MESS["IM.PAYMENT_PARTIALLY_PAYEED_STATUS"] = "���� �������� �������";
$MESS["IM.PAYMENT_PARTIALLY_PAYEED_STATUS_DESCRIPTION"] = "������� � ����� ������ ���������� �����, ����� ���� � ������� IntellectMoney �������� �������";
$MESS["IM.PAYMENT_CANSELED_STATUS"] = "���� �������";
$MESS["IM.PAYMENT_CANSELED_STATUS_DESCRIPTION"] = "������� � ����� ������ ���������� �����, ����� ���� � ������� IntellectMoney �������";
?>