<?php
$MESS["IM.PAYMENT_TITLE"] = "Payment system IntellectMoney";
$MESS["IM.PAYMENT_DESC"] = "Accept payments via credit cards, electronic purses (like Webmoney), mobile commerce";

$MESS["IM.PAYMENT_ORDER_ID"] = "Order's number";
$MESS["IM.PAYMENT_ORDER_ID_DESC"] = "Order's number at your online store";

$MESS["IM.PAYMENT_ORDER_DATE"] = "Order's date";
$MESS["IM.PAYMENT_ORDER_DATE_DESC"] = "";

$MESS["IM.PAYMENT_ORDER_SUM"] = "Amount to be paid";
$MESS["IM.PAYMENT_ORDER_SUM_DESC"] = "Choose amount that the customer shoud pay";

$MESS["IM.PAYMENT_SUCCESS_URL"] = "Success Url";
$MESS["IM.PAYMENT_SUCCESS_URL_DESC"] = "Redirects client's browser to this address on successful payment. URL should start with http:// or https://";

$MESS["IM.PAYMENT_PREFERENCE"] = "Payment preferences";
$MESS["IM.PAYMENT_PREFERENCE_DESC"] = "Comma-separated list of payment ways available to customer by default. Possible values are: inner, bankcard, exchangers. More information https://docs.google.com/document/d/1VHIkEyNgdeyqmrcVuGx0Lv8xrZp2XJZ9nYNUHDYvw0k/edit";

$MESS["IM.PAYMENT_EMAIL"] = "Email";

$MESS["IM.PAYMENT_IS_TEST"] = "Test Mode";

$MESS["IM.PAYMENT_HOLD_MODE"] = "Hold Mode";
$MESS["IM.PAYMENT_HOLD_MODE_DESCRIPTION"] = "Turn on if you want to use hold money when paying";
$MESS["IM.PAYMENT_HOLD_MODE_OFF"] = "Hold OFF";
$MESS["IM.PAYMENT_HOLD_MODE_ON"] = "Hold ON";
$MESS["IM.PAYMENT_HOLD_MODE_PERSONAL"] = "Use from personal page settings";
$MESS["IM.PAYMENT_EXPIRE_DATE"] = "Date hold in days";
$MESS["IM.PAYMENT_EXPIRE_DATE_DESCRIPTION"] = "Responsible for the life of the account. If during the life of the account, it will not be paid - the account is automatically canceled. To specify in hours. The maximum value of 180 days (4319 hours), default is 180 days. When using 'release Mode' the maximum value of 119 hours, default 72 hours";
$MESS["IM.PAYMENT_HOLD_TIME"] = "Date release in hours";
$MESS["IM.PAYMENT_HOLD_TIME_DESCRIPTION"] = "When using 'release Mode' responsible for the date of debiting. Specify in hours. The maximum value of 119 default 72 hours";

$MESS["IM.PAYMENT_OPTIONS_ESHOP_ID"] = "EshopId in IntellectMoney system";
$MESS["IM.PAYMENT_OPTIONS_ESHOP_ID_DESC"] = "Can watch on IntellectMoney.ru site";

$MESS["IM.PAYMENT_OPTIONS_SECRET_KEY"] = "Secret Key";
$MESS["IM.PAYMENT_OPTIONS_SECRET_KEY_DESC"] = "Set in when create eShop in the IntellectMoney.ru site";

$MESS["IM.PAYMENT_LANGUAGE_TITLE"] = "Language in IntellectMoney system payment page";
$MESS["IM.PAYMENT_LANGUAGE_RU"] = "Russion";
$MESS["IM.PAYMENT_LANGUAGE_EN"] = "English";
$MESS["IM.PAYMENT_LANGUAGE_DE"] = "German";
$MESS["IM.PAYMENT_LANGUAGE_FR"] = "French";
$MESS["IM.PAYMENT_LANGUAGE_IT"] = "Italian";
$MESS["IM.PAYMENT_LANGUAGE_ES"] = "Spanish";
$MESS["IM.PAYMENT_LANGUAGE_PT"] = "Portuguese";
$MESS["IM.PAYMENT_LANGUAGE_JP"] = "Japan";
$MESS["IM.PAYMENT_LANGUAGE_BG"] = "Bulgarian";

$MESS["IM.PAYMENT_SUBMIT_DELAY"] = "Delay before submit form";
$MESS["IM.PAYMENT_SUBMIT_DELAY_DESCRIPTION"] = "Delay before submit an IntellectMoney payment . Default: 5 seconds";

$MESS["IM.PAYMENT_TAX_DESC"] = "Estimated";
$MESS["IM.PAYMENT_TAX_NONE"] = "None";
$MESS["IM.PAYMENT_TAX"] = "The VAT rate for product";
$MESS["IM.PAYMENT_TAX_DESCRIPTION"] = "Specify at what rate of VAT You are working";
$MESS["IM.PAYMENT_DELIVERY_TAX"] = "The VAT rate for delivery";
$MESS["IM.PAYMENT_DELIVERY_TAX_DESCRIPTION"] = "Specify at what rate VAT delivered";
$MESS["IM.PAYMENT_GROUP"] = "The device group";
$MESS["IM.PAYMENT_GROUP_DESCRIPTION"] = "The group of devices, which will be broken check. If this parameter is left blank, will be set to default";

$MESS["IM.PAYMENT_GENERAL_GROUP"] = "Payment page settings";
$MESS["IM.PAYMENT_HOLD_GROUP"] = "Expire date and hold time";
$MESS["IM.PAYMENT_MERCHANT_RECEIPT_GROUP"] = "Cashbox online settings";
$MESS["IM.PAYMENT_ORDER_GROUP"] = "Order settings";
?>