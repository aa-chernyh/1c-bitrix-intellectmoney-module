<?php

use Bitrix\Sale;
use Bitrix\Main\Entity\EntityError;
use Bitrix\Main\Error;
use Bitrix\Main\Request;
use Bitrix\Main\Type\DateTime;
use Bitrix\Main\Web\HttpClient;
use Bitrix\Sale\PaySystem;
use Bitrix\Sale\Payment;
use Bitrix\Main\Application;
use Bitrix\Sale\PriceMaths;
use Bitrix\Sale\Result;
use Bitrix\Main\Type\Date;
use Bitrix\Main\Localization\Loc;

include 'IntellectMoneyCommon/MerchantReceiptHelper.php';

class IntellectMoney {

    protected $payment;
    protected $order;
    protected $order_id;
    protected $CSalePaySystemAction;

    protected function getCSalePaySystemAction() {
        $this->CSalePaySystemAction = new CSalePaySystemAction();
    }

    protected function getOrder() {
        $orderId = explode('/', $this->order_id);
        $this->order = Sale\Order::loadByAccountNumber($orderId[0]);
        if (is_null($this->order) || $this->order === false) {
            $this->order = Sale\Order::load($orderId[0]);
        }
    }

    protected function getPayment() {
        $paymentCollection = $this->order->getPaymentCollection();
        foreach ($paymentCollection as $payment) {
            if ($payment->getField('ACCOUNT_NUMBER') === $this->order_id) {
                $this->payment = $payment;
            }
        }
        isset($this->payment) or $this->payment = $paymentCollection->current();
    }

    protected function isIntellectMoneyProcessor() {
        $paySystem = $this->payment->getPaySystem();
        $processor = $paySystem->getField('ACTION_FILE');
        return $processor == "/bitrix/php_interface/include/sale_payment/im.payment";
    }

}

class IntellectMoneyPayment extends IntellectMoney {

    private $secret_key;
    private $eshop_id;
    private $recipient_currency;
    private $default_currency;
    private $conversion_rate;
    private $recipient_amount;
    private $preference;
    private $is_test;
    private $hold_mode;
    private $expire_date;
    private $hold_time;
    private $service_name;
    private $tax;
    private $delivery_tax;
    private $group;
    private $lang;
    private $invoice_id = null;
    private $success_url;
    private $user_email;
    private $merchant_receipt;
    private $user_name;

    function __construct() {
        Loc::loadMessages(__FILE__);
        $this->getSettings();
    }

    private function getSettings() {
        $this->getCSalePaySystemAction();
        $this->order_id = $this->CSalePaySystemAction->GetParamValue("IM.ORDER_ID");
        $this->getOrder();
        $this->getLang();
        $this->getPayment();
        $this->mapParamFromSettings();
        $this->getCurrency();
        $this->getAmount();
        if ($this->issetInvoice()) {
            $this->invoice_id = $this->payment->getField("PS_STATUS_MESSAGE");
            return true;
        }

        $this->getServiceName();
        $this->getHoldMode();
        $this->prepareHoldData();
        $this->generateHash();
        $this->getGroup();
        $this->generateMerchantReceipt();
        $this->getUserName();
    }

    private function mapParamFromSettings() {
        $this->eshop_id = $this->CSalePaySystemAction->GetParamValue("IM.ESHOP_ID");
        $this->secret_key = $this->CSalePaySystemAction->GetParamValue("IM.SEKRET_KEY");
        $this->is_test = $this->CSalePaySystemAction->GetParamValue("IM.IS_TEST") == 'Y';
        $this->expire_date = $this->CSalePaySystemAction->GetParamValue("IM.EXPIRE_DATE");
        $this->hold_time = !$this->CSalePaySystemAction->GetParamValue("IM.HOLD_TIME");
        $this->preference = $this->CSalePaySystemAction->GetParamValue("IM.PREFERENCE");
        $this->tax = $this->CSalePaySystemAction->GetParamValue("IM.TAX");
        $this->delivery_tax = $this->CSalePaySystemAction->GetParamValue("IM.DELIVERY_TAX");
        $this->success_url = $this->CSalePaySystemAction->GetParamValue("IM.SUCCESS_URL");
        $this->user_email = $this->CSalePaySystemAction->GetParamValue("IM.EMAIL");
        $this->delay = $this->CSalePaySystemAction->GetParamValue("IM.SUBMIT_DELAY");
    }

    private function issetInvoice() {
        $payment_params = $this->payment->getFieldValues();
        return (!empty($payment_params["PS_STATUS_MESSAGE"]) && $payment_params["PS_STATUS_MESSAGE"] > 3000000000);
    }

    private function getLang() {
        $lang = $this->CSalePaySystemAction->GetParamValue("IM.LANGUAGE");
        $this->lang = empty($lang) ? 'ru' : strtolower($lang);
    }

    private function getCurrency() {
        $this->recipient_currency = $this->is_test ? "TST" : $this->order->getCurrency();
    }

    private function getAmount() {
        $recipient_amount = $this->payment->getSum();
        if ($recipient_amount == null || $recipient_amount <= 0) {
            $recipient_amount = $this->order->getPrice();
        }
        $orderCurrency = strtoupper($this->order->getCurrency());
        if(!in_array($orderCurrency, array("RUB", "RUR")) && CModule::IncludeModule('currency')) {
            $converted_recipient_amount = CCurrencyRates::ConvertCurrency($recipient_amount, $orderCurrency, "RUB");
            //Вызов метода CCurrencyRates::ConvertCurrency также конвертирует цены товаров для merchantReceipt
            if($this->recipient_currency != "TST") {
                $this->recipient_currency = "RUB";
            }
            $this->default_currency = $orderCurrency;
            $this->conversion_rate = $converted_recipient_amount / $recipient_amount;
            $recipient_amount = $converted_recipient_amount;
        }
        $this->recipient_amount = number_format($recipient_amount, 2, '.', '');
    }

    private function getServiceName() {
        $this->service_name = Loc::getMessage("IM.PAYMENT_PAYMENT_FOR_ORDER", array("#DATE#" => $this->order->getField('DATE_INSERT'), "#ORDER_ID#" => $this->order_id));
    }

    private function getHoldMode() {
        $this->hold_mode = $this->CSalePaySystemAction->GetParamValue("IM.HOLD_MODE");
        !empty($this->hold_mode) or $this->hold_mode = "NONE";
    }

    private function prepareHoldData() {
        if ($this->hold_mode == "1") {
            $this->expire_date = date('Y-m-d H:i:s', strtotime('+' . $this->getHoldHours($this->expire_date) . ' hour'));
            $this->hold_time = $this->getHoldHours($this->hold_time);
        } elseif ($this->hold_mode == "0") {
            $this->expire_date = date('Y-m-d H:i:s', strtotime('+' . $this->getExpiredHours($this->expire_date) . ' hour'));
            $this->hold_time = false;
		} else {
			$this->expire_date = false;
			$this->hold_time = false;
		}
    }

    private function getExpiredHours($days) {
        return (intval($days) && $days > 0 && $days < 4319) ? $days : 4319;
    }

    private function getHoldHours($hours) {
        return (intval($hours) && $hours > 0 && $hours < 119) ? $hours : 72;
    }

    private function getGroup() {
        $this->group = $this->CSalePaySystemAction->GetParamValue("IM.GROUP");
        !empty($this->group) or $this->group = null;
    }

    private function generateMerchantReceipt() {
        $merchant_helper = new \PaySystem\MerchantReceiptHelper($this->recipient_amount, null, $this->user_email, $this->group, null, $this->order->getPrice());

        $basket = $this->order->getBasket();
        $products = $basket->getBasketItems();
        foreach ($products as $product) {
            $title = $this->convertEncoding($product->getField('NAME'));
            $merchant_helper->addItem($product->getPrice(), $product->getQuantity(), $title, $this->tax);
        }

        if ($this->order->getDeliveryPrice() > 0) {
            $title = $this->convertEncoding("Доставка");
            $merchant_helper->addItem($this->order->getDeliveryPrice(), 1, $title, $this->delivery_tax);
        }

        $this->merchant_receipt = $merchant_helper->generateMerchantReceipt();
    }

    private function convertEncoding($title) {
        if (LANG_CHARSET != "UTF-8") {
            $title = mb_convert_encoding($title, "UTF-8", "cp1251");
        }
        return $title;
    }

    private function generateHash() {
        $this->pre_hash = md5(join('::', array($this->eshop_id, $this->order_id, $this->service_name, $this->recipient_amount, $this->recipient_currency, $this->secret_key)));
    }

    private function getUserName() {
        $arFilter = Array(
            Array(
                "LOGIC" => "AND",
                Array(
                    "ID" => $this->order->getUserId()
                )
            )
        );
        $res = Bitrix\Main\UserTable::getList(
            Array(
                "select" => Array("NAME", "LAST_NAME"),
                "filter" => $arFilter
            )
        );
        $user = $res->fetch();

        $this->user_name = $user["NAME"] . " " . $user["LAST_NAME"];
    }

    function getTemplate() {
        require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/include/sale_payment/im.payment/template/template.php");
    }

}

class IntellectMoneyResult extends IntellectMoney {

    public $module_id;
    public $secret_key;
    public $eshop_id;
    public $eshop_account;
    public $recipient_currency;
    public $default_currency;
    public $conversion_rate;
    public $recipient_amount;
    public $preference;
    public $is_test;
    public $is_hold;
    public $expire_date;
    public $hold_time;
    public $service_name;
    public $user_name;
    public $user_email;
    public $payment_data;
    public $group;
    public $lang;
    public $url;
    public $payment_status;
    public $statuses;

    function __construct() {
        define("STOP_STATISTICS", true);
        define("NOT_CHECK_PERMISSIONS", true);

        if (!require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php"))
            die('prolog_before.php not found!');
        if (!CModule::IncludeModule("sale"))
            die('sale module not found');
        Loc::loadMessages(__FILE__);

        $this->module_id = "im.payment";
        $this->getParams();
        CSalePaySystemAction::InitParamArrays($this->order, $this->order->getId());
    }

    private function getParams() {
        $this->eshop_id = $_REQUEST['eshopId'];
        $this->order_id = $_REQUEST['orderId'];
        $this->service_name = $_REQUEST['serviceName'];
        $this->eshop_account = $_REQUEST['eshopAccount'];
        $this->recipient_amount = number_format($_REQUEST['recipientAmount'], 2, '.', '');
        $this->recipient_currency = strtolower($_REQUEST['recipientCurrency']);
        $this->payment_status = $_REQUEST['paymentStatus'];
        $this->user_name = $_REQUEST['userName'];
        $this->user_email = $_REQUEST['userEmail'];
        $this->payment_data = $_REQUEST['paymentData'];
        $this->secret_key = $_REQUEST['secretKey'];
        $this->payment_id = $_REQUEST['paymentId'];
        $this->hash = $_REQUEST['hash'];
        $this->default_currency = $_REQUEST['UserField_1'];
        $this->conversion_rate = $_REQUEST['UserField_2'];
        $this->getCSalePaySystemAction();
        $this->getOrder();
        if (!$this->issetOrder()) {
            Loc::GetMessage("IM.PAYMENT_WRONG_ORDER_ID", array("#ORDER_ID#" => $this->order_id));
            die;
        }
        $this->getPayment();

        $this->statuses = array(
            "2" => 0,
            "3" => 1,
            "6" => 2,
            "7" => 3,
            "5" => 4,
            "4" => 6
        );
    }

    function processingResponse() {
        if (!$this->checkIsDebug()) {
            ob_start();
            if ($this->isIntellectMoneyProcessor()) {
                if (!$this->checkIP()) {
                    $this->updateOrder(Loc::GetMessage("IM.PAYMENT_WRONG_IP", array("#ORDER_ID#" => $this->order_id)));
                    $this->obExit("IP DID NOT MATCH");
                }

                if (!$this->issetOrder()) {
                    $this->updateOrder(Loc::GetMessage("IM.PAYMENT_WRONG_ORDER_ID", array("#ORDER_ID#" => $this->order_id)));
                    $this->obExit("ORDER NOT FOUND");
                }

                if (!$this->checkPaymentCurrency()) {
                    $this->updateOrder(Loc::GetMessage("IM.PAYMENT_AMOUNT_CURRENCY_DONT_MATCH", array("#ORDER_ID#" => $this->order_id)));
                    $this->obExit("CURRENCY DID NOT MATCH! response_currency: $this->recipient_currency; order_currency:" . $this->order->getCurrency());
                }

                if (!$this->checkEshopId()) {
                    $this->updateOrder(Loc::GetMessage("IM.PAYMENT_ESHOP_ID_DONT_MATCH", array("#ORDER_ID#" => $this->order_id)));
                    $this->obExit("ERROR: INCORRECT ESHOP_ID! eshopId: $this->eshop_id; order_eshopId: " . $this->CSalePaySystemAction->GetParamValue("IM.ESHOP_ID"));
                }

                if (!$this->checkSecretKey()) {
                    $this->updateOrder(Loc::GetMessage("IM.PAYMENT_SECRET_KEY_DONT_MATCH", array("#ORDER_ID#" => $this->order_id)));
                    $this->obExit("ERROR: SECRET_KEY MISMATCH!");
                }

                $this->checkHash();
                if ($this->isChangeOrder()) {
                    $orderFields = array(
                        "USER_ID" => $this->order->getField("USER_ID")
                    );
                    $responseDate = new Date($this->payment_data, "Y-m-d H:i:s");
                    $paymentFields = array(
                        "PS_STATUS" => "N",
                        "PS_RESPONSE_DATE" => $responseDate,
                        "PS_STATUS_CODE" => $this->payment_status,
                        "PS_SUM" => $this->recipient_amount,
                        "PS_STATUS_MESSAGE" => $this->payment_id
                    );

                    if ($this->payment_status == '3') {
                        $orderFields["STATUS_ID"] = $this->CSalePaySystemAction->GetParamValue("IM.PAYMENT_CREATE_STATUS");
                    }

                    if ($this->payment_status == '4') {
                        $orderFields = array_merge(array(
                            "STATUS_ID" => $this->CSalePaySystemAction->GetParamValue("IM.PAYMENT_CANSELED_STATUS"),
                            "EMP_CANCELED_ID" => $this->order->getUserId(),
                            "REASON_CANCELED" => Loc::GetMessage("IM.PAYMENT_CANSELED"),
                        ), $orderFields);
                        $paymentFields["PS_STATUS_DESCRIPTION"] = Loc::GetMessage("IM.PAYMENT_CANSELED");
                        $this->payment->setPaid("N");
                    }

                    if ($this->payment_status == '5') {
                        $isFullPaid = $this->order->getSumPaid() == $this->order->getPrice();
                        if(!empty($this->conversion_rate)) {
                            $isFullPaid = $isFullPaid || $this->isOrderPaid($this->recipient_amount * $this->conversion_rate);
                        } else {
                            $isFullPaid = $isFullPaid || $this->isOrderPaid($this->recipient_amount);
                        }
                        
                        if ($isFullPaid) {
                            $orderFields["STATUS_ID"] = $this->CSalePaySystemAction->GetParamValue("IM.PAYMENT_PAYEED_STATUS");
                        } else {
                            $this->payment_status ='6';
                        }

                        $this->payment->setPaid('Y');
                        $paymentFields["PS_STATUS_DESCRIPTION"] = Loc::GetMessage("IM.PAYMENT_PAYMENT_FOR_ORDER_SUCCESFUL");
                    }

                    if ($this->payment_status == '6') {
                        $orderFields["STATUS_ID"] = $this->CSalePaySystemAction->GetParamValue("IM.PAYMENT_HOLD_STATUS");
                        $paymentFields["PS_STATUS_DESCRIPTION"] = Loc::GetMessage("IM.PAYMENT_HOLD", array("#ORDER_ID#" => $this->order_id));
                    }

                    if ($this->payment_status == '7') {
                        $orderFields["STATUS_ID"] = $this->CSalePaySystemAction->GetParamValue("IM.PAYMENT_PARTIALLY_PAYEED_STATUS");
                        $paymentFields["PS_STATUS_DESCRIPTION"] = Loc::GetMessage("IM.PAYMENT_PARTIALLY_PAY", array("#ORDER_ID#" => $this->order_id));
                    }

                    $this->payment->setFields($paymentFields);
                    $this->order->setFields($orderFields);
                    $this->order->save();
                }
            }
            $this->obExit();
        }

        require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
    }
    
    private function isOrderPaid($amount) {
        return 
            $amount + $this->order->getSumPaid() == $this->order->getPrice()
            || $amount == $this->order->getPrice();
    }

    private function checkIsDebug() {
        if ($_REQUEST['debug_file']) {
            if ($this->checkIP()) {
                $this->showFile();
                return true;
            }
        }
        return false;
    }

    private function showFile() {
        header('Content-type: text/plain; charset=utf-8');
        echo file_get_contents(__FILE__);
    }

    private function checkIP() {
        return in_array($_SERVER['REMOTE_ADDR'], array("194.147.107.254", "91.212.151.242", "127.0.0.1"));
    }

    private function checkPaymentCurrency() {
        $order_currency = strtoupper($this->order->getCurrency());
        return in_array($order_currency, array("RUB", "RUR", "TST")) || CModule::IncludeModule('currency');
    }

    private function updateOrder($message) {
        $this->orderFields["PS_STATUS_DESCRIPTION"] = $message;
        CSaleOrder::Update($this->order_id, $this->ar_fields_failed);
    }

    private function issetOrder() {
        return !empty($this->order);
    }

    private function checkEshopId() {
        return ($this->eshop_id == $this->CSalePaySystemAction->GetParamValue('IM.ESHOP_ID'));
    }

    private function checkSecretKey() {
        return !($this->secret_key && $this->secret_key != $this->CSalePaySystemAction->GetParamValue("IM.SEKRET_KEY"));
    }

    private function checkHash() {
        $control_hash_str = implode('::', array(
            $this->eshop_id, $this->order_id, $this->service_name,
            $this->eshop_account, $this->recipient_amount, strtoupper($this->recipient_currency), $this->payment_status,
            $this->user_name, $this->user_email, $this->payment_data, $this->CSalePaySystemAction->GetParamValue("IM.SEKRET_KEY")
        ));

        $control_hash = md5($control_hash_str);
        $control_hash_utf8 = md5(iconv('windows-1251', 'utf-8', $control_hash_str));
        if (($this->hash != $control_hash && $this->hash != $control_hash_utf8) || !$this->hash) {
            $this->updateOrder(Loc::GetMessage("IM.PAYMENT_SIGNS_DONT_MATCH", array("#ORDER_ID#" => $this->order_id)));
            $err = "ERROR: HASH MISMATCH\n";
            $err .= $this->checkIP() ? "Control hash string: $control_hash_str;\n" : "";
            $err .= "Control hash win-1251: $control_hash;\nControl hash utf-8: $control_hash_utf8;\nhash: $this->hash;\n\n";
            $this->obExit($err);
            die;
        }
    }

    private function isChangeOrder() {
        return $this->statuses[$this->getPaymentStatus()] <= $this->statuses[$this->payment_status];
    }

    private function getPaymentStatus() {
        return !empty($this->payment->getField("PS_STATUS_CODE")) ? $this->payment->getField("PS_STATUS_CODE") : "2";
    }

    private function obExit($status = null) {
        if ($status) {
            ob_end_flush();
            if (isset($_REQUEST["debug"])) {
                echo $status;
            }
            exit();
        } else {
            ob_end_clean();
            header("HTTP/1.0 200 OK");
            echo "OK";
            exit();
        }
    }

}