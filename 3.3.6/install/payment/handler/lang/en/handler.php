<?php
$MESS["IM.PAYMENT_PAYMENT_FOR_ORDER"] = "Payment for order #ORDER_ID# from #DATE#";
$MESS["IM.PAYMENT_WRONG_IP"] = "IP didn't match";
$MESS["IM.PAYMENT_PAYMENT_ID"] = "Transaction id at payment system IntellectMoney #TRANSACTION_ID#";
$MESS["IM.PAYMENT_SIGNS_DONT_MATCH"] = "Sign of order number #ORDER_ID# didn't match";
$MESS["IM.PAYMENT_AMOUNT_CURRENCY_DONT_MATCH"] = "Currency of order number #ORDER_ID# didn't match";
$MESS["IM.PAYMENT_AMOUNT_DONT_MATCH"] = "Amount of order number #ORDER_ID# didn't match";
$MESS["IM.PAYMENT_SECRET_KEY_DONT_MATCH"] = "SecretKey of order number #ORDER_ID# didn't match";
$MESS["IM.PAYMENT_ESHOP_ID_DONT_MATCH"] = "Eshop Id of order number #ORDER_ID# didn't match (during SecretKey check)";
$MESS["IM.PAYMENT_PAYMENT_FOR_ORDER_SUCCESFUL"] = "Payment for order number #ORDER_ID# completed successfully";
$MESS["IM.PAYMENT_WRONG_ORDER_ID"] = "Cannot find data for order by ID #ORDER_ID#";
$MESS["IM.PAYMENT_NOT_FULL_PAYMENT"] = "Order is partially payed";
$MESS["IM.PAYMENT_CANSELED"] = "Order canseled by payment system";
$MESS["IM.PAYMENT_PARTIALLY_PAY"] = "Order is partially payed";
$MESS["IM.PAYMENT_HOLD"] = "Amount is hold by payment system";
?>