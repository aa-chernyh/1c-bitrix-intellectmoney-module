<?php

global $MESS;

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

Class im_payment extends CModule {

    var $MODULE_ID = "im.payment";
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $MODULE_GROUP_RIGHTS = "Y";
    var $PARTNER_NAME;
    var $PARTNER_URI;

    function im_payment() {
        $path = str_replace("\\", "/", __FILE__);
        $path = substr($path, 0, strlen($path) - strlen("/index.php"));
        include($path . "/version.php");

        $this->MODULE_VERSION = $arModuleVersion["VERSION"];
        $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        $this->MODULE_NAME = Loc::getMessage("IM.PAYMENT_MODULE_NAME");
        $this->MODULE_DESCRIPTION = Loc::getMessage("IM.PAYMENT_MODULE_DESCRIPTION");
        $this->PARTNER_NAME = "IntellectMoney";
        $this->PARTNER_URI = "https://www.intellectmoney.ru";
    }

    function DoInstall() {

        global $APPLICATION;

        if (CModule::IncludeModule("sale") && SM_VERSION > '17.0.8') {
            $this->InstallFiles();

            $id = CSalePaySystemAction::Add(array(
                        'PAY_SYSTEM_ID' => '',
                        'PERSON_TYPE_ID' => '',
                        'NAME' => Loc::getMessage('IM.PAYMENT_MODULE_NAME'),
                        'PSA_NAME' => Loc::getMessage('IM.PAYMENT_MODULE_NAME'),
                        'ACTIVE' => 'Y',
                        'IS_CASH' => 'N',
                        'SORT' => 10,
                        'DESCRIPTION' => Loc::getMessage('IM.PAYMENT_MODULE_DESCRIPTION'),
                        'ACTION_FILE' => '/bitrix/php_interface/include/sale_payment/im.payment',
                        'LOGOTIP' => CFile::MakeFileArray('/bitrix/php_interface/include/sale_payment/im.payment/logo.png'),
                        'NEW_WINDOW' => 'N',
                        'HAVE_PAYMENT' => 'Y',
                        'ALLOW_EDIT_PAYMENT' => 'Y',
            ));

            RegisterModule("im.payment");
        } else {
            $APPLICATION->IncludeAdminFile(Loc::getMessage("IM.PAYMENT_INSTALL_TITLE"), $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/im.payment/install/error.php");
        }
    }

    function DoUninstall() {
        if (CModule::IncludeModule("sale")) {
            $ps = CSalePaySystemAction::GetList(null, array('ACTION_FILE' => '/bitrix/php_interface/include/sale_payment/im.payment'));
            $res = $ps->Fetch();
            while ($res) {
                CSalePaySystemAction::Delete($res['ID']);
                $res = $ps->GetNext();
            }
        }

        $this->UnInstallFiles();
        UnRegisterModule("im.payment");
    }

    function InstallFiles() {
        CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/im.payment/install/payment/", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/include/sale_payment/im.payment/", true, true);
        CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/im.payment/install/tools/", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/tools/im.payment/", true, true);
        return true;
    }

    function UnInstallFiles() {
        DeleteDirFilesEx("/bitrix/php_interface/include/sale_payment/im.payment");
        DeleteDirFilesEx("/bitrix/tools/im.payment/");
        return true;
    }

}
