<?php

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

if (!check_bitrix_sessid()) {
    return;
}

echo CAdminMessage::ShowMessage(Array("TYPE" => "ERROR", "MESSAGE" => Loc::getMessage('IM.PAYMENT_INSTALL_ERROR_TITLE'), "DETAILS" => Loc::getMessage('IM.PAYMENT_INSTALL_ERROR_DESCRIPTION'), "HTML" => true));
?>
<form action="<? echo $APPLICATION->GetCurPage() ?>">
    <p>
        <input type="hidden" name="lang" value="<? echo LANG ?>">
        <input type="submit" name="" value="<? echo Loc::getMessage('IM.PAYMENT_INSTALL_ERROR_BUTTON') ?>">
    </p>
</form>